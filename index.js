//GET
fetch('https://jsonplaceholder.typicode.com/todos/')
.then((response)=> response.json())
.then((array)=> console.log(array.map(item => item.title)));


//GET 
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=> response.json())
.then((json)=> console.log(`The item "${json.title}" on the list has a status ${json.completed}`));


//POST
fetch('https://jsonplaceholder.typicode.com/todos/', {

	method: 'POST',
	headers: {
			'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1,
		
	})
})				//converts our json response 
.then((response)=> response.json())
.then((json)=> console.log(json));


//PUT
fetch('https://jsonplaceholder.typicode.com/todos/1', 
{
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update thy my to do list with different data structure.",
		id: 1,
		title: "Updated To Do List Item",
		userId: 1 
	})

})
.then((response)=> response.json())
.then((json)=> console.log(json));



//PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', 
{
	method: 'PATCH',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21",
		
	})

})
.then((response)=> response.json())
.then((json)=> console.log(json));



//DELETE
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "DELETE"

})
.then((response)=> response.json())
.then((json)=> console.log(json));
